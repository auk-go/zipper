package integratedtests

import (
	"github.com/codingsince1985/checksum"
	"gitlab.com/evatix-go/errorwrapper/errdata/errstr"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

func singleFileMd5(file string) *errstr.Result {
	md5, err := checksum.MD5sum(file)

	if err != nil {
		errWp := errnew.Path.Messages(
			errtype.Hash,
			file,
			err.Error(),
			"singleFileMd5 -> failed to generate hash.",
		)

		return errstr.New.Result.ErrorWrapper(errWp)
	}

	return errstr.New.Result.ValueOnly(md5)
}
