package strhelperinternal

import "gitlab.com/evatix-go/core/constants"

func MissingElement(arr1, arr2 []string) string {
	missingString := constants.EmptyString
	for _, str := range arr1 {
		if notContains(str, arr2) {
			missingString = str
			return missingString
		}
	}

	return missingString
}

func notContains(search string, array []string) bool {
	for _, str := range array {
		if str != search {
			return true
		}
	}

	return false
}
